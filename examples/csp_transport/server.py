import logging

import cfdp
from cfdp.constants import ChecksumType
from cfdp.transport import CspTransport
from cfdp.filestore import NativeFileStore


logging.basicConfig(level=logging.DEBUG)


config = cfdp.Config(
    local_entity=cfdp.LocalEntity(1, 17),
    remote_entities=[cfdp.RemoteEntity(2, 12, type_of_checksum=ChecksumType.IEEE)],
    filestore=NativeFileStore("../files/server"),
    transport=CspTransport(17))

cfdp_entity = cfdp.CfdpEntity(config)
cfdp_entity.transport.bind()

input("Running. Press <Enter> to stop...\n")

cfdp_entity.transport.unbind()
cfdp_entity.shutdown()
