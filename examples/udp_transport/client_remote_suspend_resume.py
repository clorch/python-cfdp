import logging
import time

import cfdp
from cfdp.transport import UdpTransport
from cfdp.filestore import NativeFileStore


logging.basicConfig(level=logging.INFO)

config = cfdp.Config(
    local_entity=cfdp.LocalEntity(2, "127.0.0.1:5552"),
    remote_entities=[cfdp.RemoteEntity(1, "127.0.0.1:5551")],
    filestore=NativeFileStore("../files/client"),
    transport=UdpTransport())

cfdp_entity = cfdp.CfdpEntity(config)

# client needs to bind to a port for server replies
cfdp_entity.transport.bind()

cfdp_entity.put(
    destination_id=1,
    transmission_mode=cfdp.TransmissionMode.UNACKNOWLEDGED,
    messages_to_user=[
        cfdp.ProxyPutRequest(
            destination_entity_id=2,
            source_filename="/medium.txt",
            destination_filename="/medium.txt")])

transaction_id = None
while transaction_id is None:
    transaction_ids = list(cfdp_entity.machines.keys())
    for n in transaction_ids:
        if n[0] == 1:
            transaction_id = n
            break  # transfer from remote has started

cfdp_entity.put(
    destination_id=1,
    transmission_mode=cfdp.TransmissionMode.UNACKNOWLEDGED,
    messages_to_user=[cfdp.RemoteSuspendRequest(*transaction_id)])

print("Transaction suspended.")
time.sleep(4)

cfdp_entity.put(
    destination_id=1,
    transmission_mode=cfdp.TransmissionMode.UNACKNOWLEDGED,
    messages_to_user=[cfdp.RemoteResumeRequest(*transaction_id)])

print("Transaction resumed.")
time.sleep(0.1)

cfdp_entity.put(
    destination_id=1,
    transmission_mode=cfdp.TransmissionMode.UNACKNOWLEDGED,
    messages_to_user=[cfdp.RemoteSuspendRequest(*transaction_id)])

print("Transaction suspended.")
time.sleep(4)

cfdp_entity.put(
    destination_id=1,
    transmission_mode=cfdp.TransmissionMode.UNACKNOWLEDGED,
    messages_to_user=[cfdp.ProxyPutCancel(*transaction_id)])

print("Transaction cancelled.")
time.sleep(1)

cfdp_entity.transport.unbind()
cfdp_entity.shutdown()
