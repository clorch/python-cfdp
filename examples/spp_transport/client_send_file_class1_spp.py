import time

import spp

import cfdp
from cfdp.transport import SppTransport
from cfdp.filestore import NativeFileStore


udp_transport = spp.UdpTransport()

spp_config = spp.Config(
    my_apid=111,
    packet_type_of_outgoing_packets=spp.PacketType.TELECOMMAND,
    routing={
        (111, None): (udp_transport, "localhost:5111"),
        (333, None): (udp_transport, "localhost:5222"),
        },
    )


config = cfdp.Config(
    local_entity=cfdp.LocalEntity(1, (111, None)),
    remote_entities=[cfdp.RemoteEntity(3, (333, None))],
    filestore=NativeFileStore("./files/client"),
    transport=SppTransport(spp_config))

cfdp_entity = cfdp.CfdpEntity(config)
cfdp_entity.transport.bind()

transaction_id = cfdp_entity.put(
    destination_id=3,
    source_filename="/medium.txt",
    destination_filename="/medium.txt",
    transmission_mode=cfdp.TransmissionMode.ACKNOWLEDGED)


while not cfdp_entity.is_complete(transaction_id):
    time.sleep(0.1)

input("Press <Enter> to finish.\n")
cfdp_entity.transport.unbind()
cfdp_entity.shutdown()
