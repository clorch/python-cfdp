import spp

import cfdp
from cfdp.transport import SppTransport
from cfdp.filestore import NativeFileStore


udp_transport = spp.UdpTransport()

spp_config = spp.Config(
    my_apid=333,
    packet_type_of_outgoing_packets=spp.PacketType.TELEMETRY,
    routing={
        (111, None): (udp_transport, "localhost:5222"),
        (333, None): (udp_transport, "localhost:5333"),
        },
    )

config = cfdp.Config(
    local_entity=cfdp.LocalEntity(3, (333, None)),
    remote_entities=[cfdp.RemoteEntity(1, (111, None))],
    filestore=NativeFileStore("./files/server"),
    transport=SppTransport(spp_config))

cfdp_entity = cfdp.CfdpEntity(config)
cfdp_entity.transport.bind()

input("Running. Press <Enter> to stop...\n")

cfdp_entity.transport.unbind()
cfdp_entity.shutdown()
