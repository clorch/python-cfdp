import spp


udp_transport = spp.UdpTransport()

config = spp.Config(
    my_apid=222,
    routing={
        (111, None): (udp_transport, "localhost:5111"),
        (222, None): (udp_transport, "localhost:5222"),
        (333, None): (udp_transport, "localhost:5333"),
        },
    )

spp_entity = spp.SpacePacketProtocolEntity(config)

spp_entity.bind()
input("Running. Press <Enter> to stop...\n")
spp_entity.unbind()
