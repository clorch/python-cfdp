import logging
import time

import cfdp
from cfdp.constants import ConditionCode, FaultHandlerAction
from cfdp.transport import UdpTransport
from cfdp.filestore import NativeFileStore


logging.basicConfig(level=logging.DEBUG)

config = cfdp.Config(
    local_entity=cfdp.LocalEntity(2, "127.0.0.1:5552"),
    remote_entities=[cfdp.RemoteEntity(1, "127.0.0.1:5551")],
    filestore=NativeFileStore("../files/client"),
    transport=UdpTransport())

cfdp_entity = cfdp.CfdpEntity(config)

# client needs to bind to a port for server replies
cfdp_entity.transport.bind()

transaction_id = cfdp_entity.put(
    destination_id=1,
    source_filename="/small.txt",
    destination_filename="/small.txt",
    transmission_mode=cfdp.TransmissionMode.ACKNOWLEDGED,
    fault_handler_overrides={
        ConditionCode.POSITIVE_ACK_LIMIT_REACHED: FaultHandlerAction.ABANDON})

# uncomment the sections below to test suspend and freeze

# cfdp_entity.suspend(transaction_id)
# time.sleep(0.1)
# input("Transaction suspended. Press enter to resume")
# cfdp_entity.resume(transaction_id)

# cfdp_entity.freeze(receiving_entity_id=1)
# time.sleep(0.1)
# input("Transaction frozen. Press enter to unfreeze")
# cfdp_entity.thaw(receiving_entity_id=1)

# cfdp_entity.suspend(transaction_id)
# time.sleep(0.1)
# input("Transaction suspended. Press enter to cancel")
# cfdp_entity.cancel(transaction_id)

while not cfdp_entity.is_complete(transaction_id):
    time.sleep(0.1)

input("Press <Enter> to finish.\n")
cfdp_entity.transport.unbind()
cfdp_entity.shutdown()
