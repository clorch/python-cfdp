import logging

import cfdp
from cfdp.transport import UdpTransport
from cfdp.filestore import NativeFileStore


logging.basicConfig(level=logging.DEBUG)


config = cfdp.Config(
    local_entity=cfdp.LocalEntity(1, "127.0.0.1:5551"),
    remote_entities=[cfdp.RemoteEntity(2, "127.0.0.1:5552")],
    filestore=NativeFileStore("../files/server"),
    transport=UdpTransport())

cfdp_entity = cfdp.CfdpEntity(config)
cfdp_entity.transport.bind()

input("Running. Press <Enter> to stop...\n")

cfdp_entity.transport.unbind()
cfdp_entity.shutdown()
