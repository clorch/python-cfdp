import threading
import select

import sys
sys.path.append ('/usr/local/lib')

import libcsp_py3 as libcsp

from . import Transport

CSP_CFDP_PORT = 14
CSP_BUFFER_SIZE = 300

class CspTransport(Transport):

    def __init__(self, address):
        super().__init__()
        libcsp.init(address, "host", "python-cfdp", "0.0.0", 100, CSP_BUFFER_SIZE)
        libcsp.zmqhub_init(address, "localhost")
        libcsp.rtable_set(0, 0, "ZMQHUB")
        libcsp.route_start_task()
        print("Routes:")
        libcsp.print_routes()

        self._incoming_pdu_thread = threading.Thread(
            target=self._incoming_pdu_handler)
        self._incoming_pdu_thread.kill = False

    def request(self, data, address):
        packet = libcsp.buffer_get(len(data))
        libcsp.packet_set_data(packet, bytearray(data))
        libcsp.sendto(0, address, CSP_CFDP_PORT, CSP_CFDP_PORT, 0, packet, 1000)
        libcsp.buffer_free(packet)

    def bind(self):
        self.sock = libcsp.socket()
        libcsp.bind(self.sock, libcsp.CSP_ANY)
        libcsp.listen(self.sock, 1)
        self._incoming_pdu_thread.start()

    def unbind(self):
        self._incoming_pdu_thread.kill = True
        self._incoming_pdu_thread.join()

    def _incoming_pdu_handler(self):
        thread = threading.currentThread()

        while not thread.kill:
            # wait for incoming connection
            conn = libcsp.accept(self.sock, 100)
            if not conn:
                continue
            
            # print("CSP connection: source=%i:%i, dest=%i:%i" % (libcsp.conn_src(conn),
            #                                                 libcsp.conn_sport(conn),
            #                                                 libcsp.conn_dst(conn),
            #                                                 libcsp.conn_dport(conn)))

            while True:
                # Read all packets on the connection
                packet = libcsp.read(conn, 100)
                if packet is None:
                    break

                if libcsp.conn_dport(conn) == CSP_CFDP_PORT:
                    data = bytearray(libcsp.packet_get_data(packet))
                    libcsp.buffer_free(packet)
                    self.indication(data)

                else:
                    # pass request on to service handler
                    libcsp.service_handler(conn, packet)

            libcsp.close(conn)
