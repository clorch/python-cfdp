import spp

from . import Transport


class SppTransport(Transport):

    def __init__(self, spp_config):
        super().__init__()
        self.spp_config = spp_config

        self.spp_entity = spp.SpacePacketProtocolEntity(spp_config)
        self.spp_entity.indication = self._spp_pdu_indication
        self._packet_sequence_count = 0

    def _get_next_packet_sequence_count(self):
        self._packet_sequence_count += 1
        return self._packet_sequence_count

    def bind(self):
        self.spp_entity.bind()

    def unbind(self):
        self.spp_entity.unbind()

    def request(self, data, address):
        apid, apid_qualifier = address
        if self.spp_config.packet_type_of_outgoing_packets is None:
            raise ValueError
        packet_type = self.spp_config.packet_type_of_outgoing_packets
        space_packet = spp.SpacePacket(
            packet_type=packet_type,
            packet_sec_hdr_flag=False,
            apid=apid,
            sequence_flags=spp.SequenceFlags.UNSEGMENTED,
            packet_sequence_count=self._get_next_packet_sequence_count(),
            packet_data_field=data
            )
        self.spp_entity.request(
            space_packet,
            space_packet.apid,
            apid_qualifier=apid_qualifier,
            qos=None,
            )

    def _spp_pdu_indication(self, space_packet, apid, apid_qualifier=None):
        data = space_packet.packet_data_field
        self.indication(data)
