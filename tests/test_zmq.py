import os
import time

import cfdp
from cfdp.transport import ZmqTransport
from cfdp.filestore import NativeFileStore


class Server:
    """
    Server used for test client

    """

    def up(self):
        self.config = cfdp.Config(
            local_entity=cfdp.LocalEntity(1, "127.0.0.1:5551"),
            remote_entities=[cfdp.RemoteEntity(2, "127.0.0.1:5552")],
            filestore=NativeFileStore("./files/server"),
            transport=ZmqTransport(),
        )
        self.cfdp_entity = cfdp.CfdpEntity(self.config)
        self.cfdp_entity.transport.bind()

    def down(self):
        self.cfdp_entity.transport.unbind()
        self.cfdp_entity.shutdown()


def test_zmq():
    server = Server()
    server.up()

    config = cfdp.Config(
        local_entity=cfdp.LocalEntity(2, "127.0.0.1:5552"),
        remote_entities=[cfdp.RemoteEntity(1, "127.0.0.1:5551")],
        filestore=NativeFileStore("./files/client"),
        transport=ZmqTransport(),
    )
    client = cfdp.CfdpEntity(config)

    client.transport.connect(remote_entity_id=1)

    transaction_id = client.put(
        destination_id=1,
        source_filename="/medium.txt",
        destination_filename="/medium.txt",
        transmission_mode=cfdp.TransmissionMode.UNACKNOWLEDGED,
    )

    while not client.is_complete(transaction_id):
        time.sleep(0.1)

    time.sleep(0.1)
    client.shutdown()
    server.down()

    time.sleep(0.1)
    assert os.path.isfile("./files/server/medium.txt")
    os.remove("./files/server/medium.txt")


if __name__ == "__main__":
    import logging
    logging.basicConfig(level=logging.DEBUG)

    print("Test Zmq Transport " + 50 * "=")
    test_zmq()
