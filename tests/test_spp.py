import os
import time

import cfdp
import spp
from cfdp.transport import SppTransport
from cfdp.filestore import NativeFileStore


class Server:
    """
    Server used for test client

    """

    def up(self):
        udp_transport = spp.UdpTransport()
        self.spp_config = spp.Config(
            my_apid=111,
            packet_type_of_outgoing_packets=spp.PacketType.TELEMETRY,
            routing={
                (111, None): (udp_transport, "localhost:5111"),
                (222, None): (udp_transport, "localhost:5222"),
            },
        )
        self.config = cfdp.Config(
            local_entity=cfdp.LocalEntity(1, (111, None)),
            remote_entities=[cfdp.RemoteEntity(2, (222, None))],
            filestore=NativeFileStore("./files/server"),
            transport=SppTransport(self.spp_config),
        )

        self.cfdp_entity = cfdp.CfdpEntity(self.config)
        self.cfdp_entity.transport.bind()

    def down(self):
        self.cfdp_entity.transport.unbind()
        self.cfdp_entity.shutdown()


def test_spp():
    server = Server()
    server.up()

    udp_transport = spp.UdpTransport()
    spp_config = spp.Config(
        my_apid=222,
        packet_type_of_outgoing_packets=spp.PacketType.TELECOMMAND,
        routing={
            (111, None): (udp_transport, "localhost:5111"),
            (222, None): (udp_transport, "localhost:5222"),
        },
    )

    config = cfdp.Config(
        local_entity=cfdp.LocalEntity(2, (222, None)),
        remote_entities=[cfdp.RemoteEntity(1, (111, None))],
        filestore=NativeFileStore("./files/client"),
        transport=SppTransport(spp_config),
    )

    client = cfdp.CfdpEntity(config)
    client.transport.bind()

    transaction_id = client.put(
        destination_id=1,
        source_filename="/medium.txt",
        destination_filename="/medium.txt",
        transmission_mode=cfdp.TransmissionMode.ACKNOWLEDGED,
    )

    while not client.is_complete(transaction_id):
        time.sleep(0.1)

    time.sleep(0.1)
    client.transport.unbind()
    client.shutdown()
    server.down()

    time.sleep(0.1)
    assert os.path.isfile("./files/server/medium.txt")
    os.remove("./files/server/medium.txt")


if __name__ == "__main__":
    import logging

    logging.basicConfig(level=logging.DEBUG)

    print("Test Spp Transport " + 50 * "=")
    test_spp()
